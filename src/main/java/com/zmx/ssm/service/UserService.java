package com.zmx.ssm.service;

import com.zmx.ssm.pojo.User;

import java.util.List;

/**
 * @author mx_designer
 * @date 2020/10/23
 */
public interface UserService {
    /**
     * 增加
     * @param user 用户
     * @return 接口
     */
    int save(User user) throws Exception;

    /**
     * 查询所有
     * @return userList
     * @throws Exception 异常
     */
    List<User> findAll() throws Exception;
}
