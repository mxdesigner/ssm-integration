package com.zmx.ssm.service.impl;

import com.zmx.ssm.mapper.UserMapper;
import com.zmx.ssm.pojo.User;
import com.zmx.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 业务层实现
 * @author mx_designer
 * @date 2020/10/23
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public int save(User user) throws Exception{
        return userMapper.save(user);
    }

    @Override
    public List<User> findAll() throws Exception {
        return userMapper.findAll();
    }
}
