package com.zmx.ssm.mapper;

import com.zmx.ssm.pojo.User;

import java.util.List;

/**
 * 用户的mapper接口
 * @author mx_designer
 * @date 2020/10/22
 */
public interface UserMapper {
    /**
     * 插入用户
     * @param user 用户
     * @return 接口
     */
    int save(User user);

    /**
     * 查询所有
     * @return userList
     */
    List<User> findAll();
}
