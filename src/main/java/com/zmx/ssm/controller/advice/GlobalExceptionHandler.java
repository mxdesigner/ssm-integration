package com.zmx.ssm.controller.advice;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author mx_designer
 * @date 2020/10/23
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ModelAndView selectException(Exception e){
        ModelAndView modelAndView = new ModelAndView();
        if (e instanceof RuntimeException){
            modelAndView.setViewName("error");
            modelAndView.addObject("errorMsg","服务器正忙，请稍后再试");
        }
        return modelAndView;
    }

}
