package com.zmx.ssm.controller;

import com.zmx.ssm.pojo.User;
import com.zmx.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author mx_designer
 * @date 2020/10/23
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/list")
    public ModelAndView list() throws Exception {
        System.out.println("list---------------");
        List<User> list = userService.findAll();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("user-list");
        modelAndView.addObject("users",list);
        return modelAndView;
    }

}
