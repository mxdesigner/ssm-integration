package com.zmx.ssm.pojo;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 用户的实体类
 * @author  mx_designer
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {


	private long id;

	private String loginName;

	private String nickname;

	private String password;

	private int gender;

	private String phone;

	private String email;

	private double balance;

	private int status;

	private Date registTime;

	private Date updateTime;
}
