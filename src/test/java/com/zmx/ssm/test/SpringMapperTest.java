package com.zmx.ssm.test;

import com.zmx.ssm.mapper.UserMapper;
import com.zmx.ssm.pojo.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

/**
 * @author mx_designer
 * @date 2020/10/22
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:spring-*.xml"})
public class SpringMapperTest {

    @Autowired
    UserMapper userMapper;

    /**
     * 测试增加
     * @throws Exception
     */
    @Test
    public void test1() throws Exception{
        User user = new User();
        user.setLoginName("zmx");
        user.setPassword("123123");
        user.setGender(1);
        user.setEmail("32683299@qq.com");
        user.setBalance(6666.66);
        user.setNickname("铭想");
        user.setPhone("1804254424");
        user.setStatus(0);
        user.setRegistTime(new Date());
        user.setUpdateTime(new Date());
        int len = userMapper.save(user);
        System.out.println(len);
    }

}
